package org.academiadecodigo.javabank;

import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Main {

    public static void main(String[] args) {

        AccountManager accountManager = new AccountManager();
        Bank bank = new Bank(accountManager);
        Customer customer1 = new Customer();
        bank.addCustomer(customer1);

        Customer customer2 = new Customer();
        bank.addCustomer(customer2);

        BankApplication bankApplication = new BankApplication(bank);
        bankApplication.start();

        //TODO ver se é possivel colocar as logicas de saque e deposito na interface

    }
}
