package org.academiadecodigo.javabank;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.operations.*;
import org.academiadecodigo.javabank.managers.AccountManager;

import java.util.HashMap;
import java.util.Map;

public class BankApplication {

    private Map<Integer, OperationHandler> operations;
    private String[] menuOptions;
    private int answerIndex;
    private int customerNumber;
    private AccountManager accountManager = new AccountManager();
    private Bank bank;

    public BankApplication(Bank bank) {
        operations = new HashMap<>();
        this.bank = bank;
        addOperations();

    }

    private void addOperations() {

        menuOptions = new String[Operation.values().length];

        for (int i = 0; i < menuOptions.length; i++) {
            operations.put(i+1, Operation.values()[i].getOperation());
            menuOptions[i] = operations.get(i + 1).toString();
        }
    }

    public void start() {

        Prompt prompt = new Prompt(System.in, System.out);
        MenuInputScanner menu = new MenuInputScanner(menuOptions);

        while(true) {

            int tempCostumerID = login(prompt);

            menu.setMessage("Welcome to Java Bank");

            showMainMenu(prompt, tempCostumerID, menu);
        }
    }

    private void showMainMenu(Prompt prompt, int tempCostumerID, MenuInputScanner menu) {
        answerIndex = prompt.getUserInput(menu);

        if (answerIndex == Operation.QUIT.getMenuIndex()){
            System.out.println("Thanks. See you next time.");
            return;
        }

        operations.get(answerIndex).execute(prompt, tempCostumerID, bank);
        menu.setMessage("Choose a new operation:");
        showMainMenu(prompt, tempCostumerID, menu);

    }

    private int login(Prompt prompt) {

        IntegerInputScanner receivedCustomerNumber = new IntegerInputScanner();
        receivedCustomerNumber.setMessage("Please insert your customer number:");
        receivedCustomerNumber.setError("Wrong number, please try again.");
        customerNumber = prompt.getUserInput(receivedCustomerNumber);

        bank.getCustomer(customerNumber);

        if (!bank.isAValidID(customerNumber)) {
            System.out.println("Wrong number, please try again.");
            customerNumber = login(prompt);
        }

        return customerNumber;

    }

}
