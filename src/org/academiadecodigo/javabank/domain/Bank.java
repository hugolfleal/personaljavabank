package org.academiadecodigo.javabank.domain;

import org.academiadecodigo.javabank.domain.account.AccountType;
import org.academiadecodigo.javabank.managers.AccountManager;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * The bank entity
 */
public class Bank {

    private AccountManager accountManager;
    private Set<Customer> customers = new HashSet<>();

    /**
     * Creates a new instance of Bank and initializes it with the given account manager
     *
     * @param accountManager the account manager
     */
    public Bank(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

    /**
     * Adds a new customer to the bank
     *
     * @param customer the new bank customer
     * @see Customer#setAccountManager(AccountManager)
     */
    public void addCustomer(Customer customer) {
        customers.add(customer);
        customer.setNumber(customers.size());
        customer.setAccountManager(accountManager);
    }

    /**
     * Gets the total balance of the bank
     *
     * @return the bank total balance
     */
    public double getBalance() {

        double balance = 0;

        for (Customer customer : customers) {
            balance += customer.getBalance();
        }

        return balance;
    }

    public int getCustomersSize() {
        return customers.size();
    }

    public Customer getCustomer(int id) {

        if (id - 1 > customers.toArray().length || id == 0) {
            return null;
        }

        return (Customer) customers.toArray()[id - 1];

    }

    public boolean isAValidID(int id) {
        return id <= customers.size();
    }

    public int openAccount(int id, AccountType accountType) {

        if (id - 1 > customers.toArray().length || id <= 0) {
            return 0;
        }

        Customer customer = (Customer)customers.toArray()[id-1];
        return customer.openAccount(accountType);
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }
}
