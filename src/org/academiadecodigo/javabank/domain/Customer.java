package org.academiadecodigo.javabank.domain;

import org.academiadecodigo.javabank.domain.account.Account;
import org.academiadecodigo.javabank.domain.account.AccountType;
import org.academiadecodigo.javabank.managers.AccountManager;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The customer domain entity
 */
public class Customer {

    private AccountManager accountManager;
    private Map<Integer, Account> accounts = new HashMap<>();
    private int number;


    /**
     * Sets the account manager
     *
     * @param accountManager the account manager to set
     */
    public void setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
    }


    /**
     * Opens a new account
     *
     * @param accountType the account type to be opened
     * @return the new account id
     * @see AccountManager#openAccount(AccountType)
     */
    public int openAccount(AccountType accountType) {
        Account account = accountManager.openAccount(accountType);
        accounts.put(account.getId(), account);
        return account.getId();
    }

    /**
     * Gets the balance of an {@link Account}
     *
     * @param id the id of the account
     * @return the account balance
     */
    public double getBalance(int id) {
        return accounts.get(id).getBalance();
    }

    /**
     * Gets the total customer balance
     *
     * @return the customer balance
     */
    public double getBalance() {

        double balance = 0;

        for (Account account : accounts.values()) {
            balance += account.getBalance();
        }

        return balance;
    }


    public void setNumber(int number) {
        this.number = number;
    }

    public AccountManager getAccountManager() {
        return this.accountManager;
    }

    public boolean isMyAccount(int id) {

        return accounts.containsKey(id);

        /*for (int i = 0; i < accounts.values().size(); i++) {

            Account account = (Account) accounts.values().toArray()[i];
            int temp = account.getId();

            if (id == temp) {
                return true;
            }
        }
        return false;*/
    }

    public Account getAccount(int id) {

        for (Account a : accounts.values()) {
            if (a.getId() == id) {
                return a;
            }
        }

        return null;
    }

    public int[] getAccountIDs() {

        int[] tempAccountIDs = new int[accounts.size()];

        Set<Integer> tempSet = accounts.keySet();

        for (int i = 0; i < tempAccountIDs.length; i++) {
            tempAccountIDs[i] = (int) tempSet.toArray()[i];
        }
        return tempAccountIDs;
    }
}
