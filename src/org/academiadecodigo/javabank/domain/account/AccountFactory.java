package org.academiadecodigo.javabank.domain.account;

public class AccountFactory {

    public Account getNewAccount(AccountType accountType, int numberAccounts){

        Account newAccount;

        if (accountType == AccountType.CHECKING) {
            newAccount = new CheckingAccount(numberAccounts);

        } else {
            newAccount = new SavingsAccount(numberAccounts);
        }

        return newAccount;
    }
}
