package org.academiadecodigo.javabank.domain.account;

/**
 * The possible {@link Account} types
 */
public enum AccountType {

    /**
     * @see CheckingAccount
     */
    CHECKING("CHECKING"),

    /**
     * @see SavingsAccount
     */
    SAVINGS("SAVINGS");

    private String str;

    AccountType (String str){
        this.str = str;
    }

    public String getString(){
        return this.str;
    }
}
