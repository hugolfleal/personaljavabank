package org.academiadecodigo.javabank.domain.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.account.AccountType;

public class OpenAccount implements OperationHandler {


    @Override
    public void execute(Prompt prompt, int customerID, Bank bank) {

        String[] accountTypes = new String[AccountType.values().length];


        for (int i = 0; i < accountTypes.length; i++) {
            accountTypes[i] = AccountType.values()[i].getString();
        }

        MenuInputScanner menuOpenAccount = new MenuInputScanner(accountTypes);
        menuOpenAccount.setMessage("Choose an account type:");
        int accountType = prompt.getUserInput(menuOpenAccount);

        int accountNumber = bank.openAccount(customerID, AccountType.values()[accountType-1]);

        System.out.println("Your account number is: " + accountNumber);

    }

    @Override
    public String toString() {
        return "Open Account";
    }
}
