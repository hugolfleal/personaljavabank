package org.academiadecodigo.javabank.domain.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.precisiondouble.DoubleRangeInputScanner;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.account.Account;
import org.academiadecodigo.javabank.domain.account.AccountType;

public class MakeWithdrawal implements OperationHandler {


    @Override
    public void execute(Prompt prompt, int customerID, Bank bank) {

        int maxTries = 3;
        int counter = maxTries;
        int counterAmount = maxTries;
        int id = getAccountID(prompt, customerID, bank, counter);

        if (id == 0) {
            return;
        }

        Account account = bank.getCustomer(customerID).getAccount(id);

        double amount = getAmount(prompt, account, counterAmount);

        if (amount == 0) {
            return;
        }

        bank.getCustomer(customerID).getAccountManager().withdraw(id, amount);

        System.out.println("You balance now is: " + account.getBalance());

    }

    private double getAmount(Prompt prompt, Account account, int counterAmount) {

        DoubleRangeInputScanner amountIN = new DoubleRangeInputScanner(0.01, 1000000);
        amountIN.setMessage("Type the amount:");
        amountIN.setError("Wrong amount, please try again.");

        double amount = prompt.getUserInput(amountIN);

        if (amount > account.getBalance()){

            System.out.println("You do not have money enough.");

            counterAmount--;

            if (counterAmount == 0){
                return counterAmount;
            }
            amount = getAmount(prompt, account, counterAmount);
        }

        return amount;

    }

    private int getAccountID(Prompt prompt, int customerID, Bank bank, int counter) {

        IntegerInputScanner id = new IntegerInputScanner();
        id.setMessage("Please insert the account number:");
        id.setError("Wrong number, please try again.");

        int idNumber = prompt.getUserInput(id);

        AccountType accountType = bank.getAccountManager().getAccountType(idNumber);

        if (!bank.getCustomer(customerID).isMyAccount(idNumber) || accountType == AccountType.SAVINGS || accountType == null) {

            System.out.println("Wrong number or type of account not allowed to withdrawal.");

            counter--;

            if (counter == 0) {
                return counter;
            }

            idNumber = getAccountID(prompt, customerID, bank, counter);
        }

        return idNumber;
    }

    @Override
    public String toString() {
        return "Make Withdrawal";
    }
}
