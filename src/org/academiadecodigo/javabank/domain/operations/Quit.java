package org.academiadecodigo.javabank.domain.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.javabank.domain.Bank;

public class Quit implements OperationHandler {
    @Override
    public void execute(Prompt prompt, int customerID, Bank bank) {

    }

    @Override
    public String toString(){
        return "Quit";
    }
}
