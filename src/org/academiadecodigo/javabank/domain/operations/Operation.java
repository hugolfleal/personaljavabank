package org.academiadecodigo.javabank.domain.operations;

public enum Operation {

    VIEW_BALANCE(1, new ViewBalance()),
    MAKE_DEPOSIT(2, new MakeDeposit()),
    MAKE_WITHDRAWAL(3, new MakeWithdrawal()),
    OPEN_ACCOUNT(4, new OpenAccount()),
    QUIT(5, new Quit());

    private final OperationHandler operation;
    private final int menuIndex;

    Operation(int menuIndex, OperationHandler operation) {
        this.menuIndex = menuIndex;
        this.operation = operation;
    }

    public OperationHandler getOperation(){
        return operation;
    }

    public int getMenuIndex(){
        return menuIndex;
    }
}
