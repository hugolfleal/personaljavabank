package org.academiadecodigo.javabank.domain.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.account.Account;

public class ViewBalance implements OperationHandler {


    @Override
    public void execute(Prompt prompt, int customerID, Bank bank) {

        int[] tempAccountIDs = bank.getCustomer(customerID).getAccountIDs();

        System.out.println("Your balance is: ");

        for (int i = 0; i < tempAccountIDs.length; i++) {

            System.out.println("Account: " + tempAccountIDs[i] + " - Balance: "
                    + bank.getCustomer(customerID).getAccount(tempAccountIDs[i]).getBalance());
        }
        System.out.println("Total balance: " + bank.getCustomer(customerID).getBalance());
    }

    @Override
    public String toString() {
        return "View Balance";
    }
}
