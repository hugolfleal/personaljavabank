package org.academiadecodigo.javabank.domain.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.precisiondouble.DoubleRangeInputScanner;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.domain.account.Account;

public class MakeDeposit implements OperationHandler {
    @Override
    public void execute(Prompt prompt, int customerID, Bank bank) {

        int maxTries = 3;
        int counter = maxTries;
        int id = getAccountID(prompt, bank, customerID, counter);

        if (id == 0) {
            return;
        }

        Account account = bank.getCustomer(customerID).getAccount(id);

        double amount = getAmount(prompt);

        bank.getCustomer(customerID).getAccountManager().deposit(id, amount);
        System.out.println("You balance now is: " + account.getBalance());
    }

    private double getAmount(Prompt prompt) {
        DoubleRangeInputScanner amount = new DoubleRangeInputScanner(0.01, 1000000);
        amount.setMessage("Type the amount:");
        amount.setError("Wrong amount, please try again.");
        return prompt.getUserInput(amount);

    }

    private int getAccountID(Prompt prompt, Bank bank, int customerID, int counter) {

        IntegerInputScanner id = new IntegerInputScanner();
        id.setMessage("Please insert the account number:");
        id.setError("Wrong number, please try again.");

        int idNumber = prompt.getUserInput(id);


        if (!bank.getCustomer(customerID).isMyAccount(idNumber)) {

            System.out.println("Wrong number.");

            counter--;

            if (counter == 0) {
                return counter;
            }

            idNumber = getAccountID(prompt, bank, customerID, counter);
        }


        return idNumber;
    }

    @Override
    public String toString() {
        return "Make Deposit";
    }
}
