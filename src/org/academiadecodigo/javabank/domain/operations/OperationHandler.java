package org.academiadecodigo.javabank.domain.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.javabank.domain.Bank;

public interface OperationHandler {

     void execute(Prompt prompt, int customerID, Bank bank);

}
